import { VRButton } from "three/examples/jsm/webxr/VRButton.js";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";
import { Client } from "colyseus.js";
import {
  WebGLRenderer,
  VSMShadowMap,
  Mesh,
  BoxGeometry,
  TorusGeometry,
  MeshBasicMaterial,
  MeshStandardMaterial,
  MOUSE,
  Scene,
  FogExp2,
  PlaneGeometry,
  DirectionalLight,
  AmbientLight,
  PerspectiveCamera,
  AudioContext,
  AudioListener,
  PositionalAudio,
  Raycaster,
  Vector3,
  Vector2,
  Euler,
  Quaternion,
  Object3D,
  TextureLoader,
  Clock,
  RepeatWrapping,
  FrontSide,
} from "three";
import * as THREE from "three";
window.THREE = THREE;
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import Peer from "peerjs";
import TWEEN from "@tweenjs/tween.js";

import AmmoWorld from "./AmmoWorld.js";
import HitSpriteSystem from "./HitSpriteSystem.js";
import { Button, ButtonGroup } from "./buttons.js";
import Hud from "./Hud.js";
import { Text } from "./text.js";
import { setToWorldSpace, projectToFloor } from "./math.js";
import { loadSkin, loadFromSkinId, loadSkinFromDataTransfer } from "./skin.js";
/* eslint-disable no-unused-vars */
import { watchFiles, printParents } from "./dev.js";

function makeCube(size, color, shadows = true, basic = false) {
  const material = basic ? new MeshBasicMaterial({ color }) : new MeshStandardMaterial({ color });
  size = typeof size === "number" ? [size, size, size] : size;
  const cube = new Mesh(new BoxGeometry(size[0], size[1], size[2]), material);
  cube.name = "cube";
  if (shadows) {
    cube.castShadow = true;
    cube.receiveShadow = true;
  }
  return cube;
}

const collisionGroups = {
  all: -1,
  enemies: 2 << 5,
  weapons: 2 << 6,
};

const enemyTypes = [
  { type: "normal", cost: 100, model: "grey" },
  { type: "fast", cost: 200, model: "red" },
];
const enemyByType = {};
for (const enemyType of enemyTypes) {
  enemyByType[enemyType.type] = enemyType;
}

const loadModel = (() => {
  const gltfLoader = new GLTFLoader();
  return (url) => {
    return new Promise((resolve, reject) => {
      gltfLoader.load(url, (gltf) => {
        resolve(gltf);
      });
    });
  };
})();

const updateBody = (() => {
  const vec = new Vector3();
  const quat = new Quaternion();
  return (body, obj, getWorldSpace) => {
    if (getWorldSpace) {
      obj.getWorldPosition(vec);
      obj.getWorldQuaternion(quat);
      body.setPosition(vec);
      body.setQuaternion(quat);
    } else {
      body.setPosition(obj.position);
      body.setQuaternion(obj.quaternion);
    }
  };
})();

class Game {
  makePointer() {
    const pointer = makeCube(0.1, "orange");
    this.scene.add(pointer);
    return pointer;
  }

  async makePlayer() {
    const player = new Object3D();
    player.name = "player";
    this.scene.add(player);

    const gltf = await loadModel("assets/avatar.glb");

    player.add(gltf.scene);

    const avatarMesh = gltf.scene.getObjectByName("Avatar");
    avatarMesh.material.side = FrontSide;
    avatarMesh.castShadow = true;
    avatarMesh.receiveShadow = true;

    const origAvatarMat = avatarMesh.material;
    player.userData.origAvatarMat = origAvatarMat;
    player.userData.basicAvatarMat = new MeshBasicMaterial({
      map: origAvatarMat.map,
      alphaTest: 0.5,
      skinning: true,
    });

    /*

                             Scene <G>
                                 +
                                 |
                                 v
                           AvatarRoot <O>
                                 +
                                 |
                    Hips <B> <-------> Avatar <SM>
                       +
                       |
                       v
                    Spine <B>
                       +
                       |
      LeftHand <B> <-------> RightHand <B>
                       |
                       v
                   Neck <B>
                       +
                       |
                       v
                   Head <B>

    */

    const avatarRoot = gltf.scene.getObjectByName("AvatarRoot");
    avatarRoot.userData.head = avatarRoot.getObjectByName("Head");
    avatarRoot.userData.hips = avatarRoot.getObjectByName("Hips");
    avatarRoot.userData.leftHand = avatarRoot.getObjectByName("LeftHand");
    avatarRoot.userData.rightHand = avatarRoot.getObjectByName("RightHand");

    const leftGrip = new Object3D();
    leftGrip.position.y = 0.06;
    leftGrip.scale.x = -1;
    avatarRoot.userData.leftHand.add(leftGrip);
    avatarRoot.userData.leftGrip = leftGrip;

    const rightGrip = new Object3D();
    rightGrip.position.y = 0.06;
    avatarRoot.userData.rightHand.add(rightGrip);
    avatarRoot.userData.rightGrip = rightGrip;

    player.userData.avatarMesh = avatarMesh;
    player.userData.avatarRoot = avatarRoot;
    player.userData.avatarLoaded = true;

    return player;
  }

  makeEnemy(position, quaternion, type) {
    const enemy = new Object3D();
    enemy.name = "enemy " + type;

    loadModel("assets/enderman.glb").then((gltf) => {
      const enderman = gltf.scene;
      enderman.scale.setScalar(0.6);
      enemy.add(enderman);
      enemy.userData.mesh = enderman.getObjectByProperty("type", "Mesh");
    });

    this.scene.add(enemy);

    const body = this.world.add({
      group: collisionGroups.enemies,
      mask: collisionGroups.all,
      size: [0.4, 2, 0.4],
      mass: 1,
      pos: [position.x, position.y, position.z],
      rot: [quaternion.x, quaternion.y, quaternion.z, quaternion.w],
      kinematic: true,
    });

    enemy.userData.body = body;

    return enemy;
  }

  makeRing(radius) {
    const ring = new Mesh(new TorusGeometry(radius, 0.1, 4, 32), new MeshStandardMaterial({ color: "black" }));
    ring.name = "ring";
    ring.rotation.x = Math.PI / 2;
    this.scene.add(ring);
  }

  addRemoteStream(clientSessionId, remoteStream) {
    const audio = new PositionalAudio(this.listener);
    audio.autoplay = true;
    audio.setMediaStreamSource(remoteStream);
    this.playerObjs[clientSessionId].add(audio);
  }

  async connectClient(stream) {
    const join = document.getElementById("join");
    join.textContent = "JOINING";

    const protocol = location.protocol === "http:" ? "ws" : "wss";
    const path = location.pathname.replace(/\/$/, "");
    const client = new Client(`${protocol}://${location.host}${path}`);

    const _room = await client.joinOrCreate("GameRoom");

    join.style.display = "none";
    const vrButton = VRButton.createButton(this.renderer);
    vrButton.style.position = "";
    vrButton.style.fontSize = "";
    vrButton.style.padding = "";
    vrButton.style.width = "";
    vrButton.style.opacity = "";
    vrButton.style.background = "";
    document.getElementById("buttons").append(vrButton);

    this.room = _room;

    console.log("joined room-", this.room.id, "clientSessionId", this.room.sessionId);

    const peer = new Peer("bp-multivr-ypptmcyiei-" + this.room.sessionId);
    peer.on("call", (call) => {
      if (stream) {
        call.answer(stream);
      }
      call.on("stream", (remoteStream) => {
        this.addRemoteStream(call.metadata.clientSessionId, remoteStream);
      });
    });

    this.room.state.players.onAdd = async (player, clientSessionId) => {
      if (clientSessionId === this.room.sessionId) return;

      this.playerObjs[clientSessionId] = await this.makePlayer();

      if (stream) {
        const call = peer.call("bp-multivr-ypptmcyiei-" + clientSessionId, stream, {
          metadata: { clientSessionId: this.room.sessionId },
        });
        if (call) {
          call.on("stream", (remoteStream) => {
            this.addRemoteStream(clientSessionId, remoteStream);
          });
        }
      }
    };

    this.room.state.players.onRemove = (player, clientSessionId) => {
      this.scene.remove(this.playerObjs[clientSessionId]);
      delete this.playerObjs[clientSessionId];
    };

    this.room.onLeave(() => {
      setTimeout(() => location.reload(), 2000);
    });
  }

  resize() {
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.renderer.setSize(w, h);
    this.camera.aspect = w / h;
    this.camera.updateProjectionMatrix();
  }

  pulseController(index) {
    const xr = this.renderer.xr;
    const inputSource = xr.isPresenting && xr.getSession().inputSources[index];
    const actuators = inputSource && inputSource.gamepad.hapticActuators;
    if (actuators && actuators.length) {
      actuators[0].pulse(0.2, 100);
    }
  }

  detectWeaponContact = (() => {
    const contactPoint = new Vector3();
    const contactPointOffset = new Vector3();

    return (body, localPlayerPosition, controllerIndex, damage) => {
      if (body.gainedContact()) {
        const { lastContact } = body;
        contactPoint.set(lastContact.x, lastContact.y, lastContact.z);

        contactPointOffset.copy(localPlayerPosition);
        contactPointOffset.sub(contactPoint);
        contactPointOffset.normalize();
        contactPointOffset.multiplyScalar(0.1);
        contactPointOffset.add(contactPoint);

        this.hitSpriteSystem.display(damage, contactPointOffset.x, contactPointOffset.y, contactPointOffset.z);

        this.power = Math.min(this.power + damage, 9999);

        this.pulseController(controllerIndex);

        // See setUserIndex
        const enemyIndex = body.lastContactBodyUserIndex;
        this.room.send("enemyHit", { index: enemyIndex, damage });
      }
    };
  })();

  updateEnemyObjFromState(obj, state) {
    if (obj.visible && !obj.userData.dying && !state.alive) {
      if (obj.userData.mesh) {
        if (!state.alive) {
          this.world.remove(obj.userData.body);
        }
        obj.userData.dying = true;
        obj.userData.mesh.material.transparent = true;
        new TWEEN.Tween(obj.userData.mesh.material)
          .to({ opacity: 0 }, 300)
          .start()
          .onComplete(() => {
            obj.visible = state.alive;
          });
      }
    }
    if (state.alive) {
      obj.position.copy(state.position);
      obj.quaternion.copy(state.quaternion);
      obj.userData.body.setPosition({
        x: obj.position.x,
        y: obj.position.y + 1,
        z: obj.position.z,
      });
      obj.userData.body.setQuaternion(obj.quaternion);
    }
  }

  updateRoomState = (() => {
    const localPlayerState = {
      skin: "",
      position: [],
      quaternion: [],
      controller0Position: [],
      controller0Quaternion: [],
      controller1Position: [],
      controller1Quaternion: [],
    };
    const tempVector = new Vector3();
    const leftPositionOffset = new Vector3(0.08, -0.09, 0.0);
    const leftRotationOffset = new Quaternion().setFromEuler(
      new Euler(90 * (Math.PI / 180), -90 * (Math.PI / 180), -40 * (Math.PI / 180), "XYZ")
    );
    const rightPositionOffset = new Vector3(-0.08, -0.09, 0.0);
    const rightRotationOffset = new Quaternion().setFromEuler(
      new Euler(90 * (Math.PI / 180), 90 * (Math.PI / 180), 40 * (Math.PI / 180), "XYZ")
    );

    return () => {
      const inVRMode = this.renderer.xr.isPresenting;

      let localPlayerPosition;
      if (inVRMode) {
        const vrCamera = this.renderer.xr.getCamera(this.camera);
        vrCamera.position.toArray(localPlayerState.position);
        vrCamera.quaternion.toArray(localPlayerState.quaternion);
        this.controller0.position.toArray(localPlayerState.controller0Position);
        this.controller0.quaternion.toArray(localPlayerState.controller0Quaternion);
        this.controller1.position.toArray(localPlayerState.controller1Position);
        this.controller1.quaternion.toArray(localPlayerState.controller1Quaternion);
        if (this.playerObj.userData.avatarLoaded) {
          tempVector.copy(vrCamera.position);
          tempVector.y -= 0.6;
          this.playerObj.position.copy(tempVector);
          this.playerObj.quaternion.copy(projectToFloor(vrCamera.quaternion));
          const avatarRoot = this.playerObj.userData.avatarRoot;
          setToWorldSpace(avatarRoot.userData.head, vrCamera.position, vrCamera.quaternion);
          setToWorldSpace(
            avatarRoot.userData.leftHand,
            this.controller1.position,
            this.controller1.quaternion,
            leftPositionOffset,
            leftRotationOffset
          );
          setToWorldSpace(
            avatarRoot.userData.rightHand,
            this.controller0.position,
            this.controller0.quaternion,
            rightPositionOffset,
            rightRotationOffset
          );
        }
        localPlayerPosition = vrCamera.position;
      } else {
        this.camera.position.toArray(localPlayerState.position);
        this.camera.quaternion.toArray(localPlayerState.quaternion);
        localPlayerPosition = this.camera.position;
      }

      if (this.playerObj.userData.avatarLoaded) {
        const image = this.playerObj.userData.avatarMesh.material.map.image;
        const mapSrc = image && image.src;
        if (mapSrc) {
          localPlayerState.skin = mapSrc;
        }
      }

      this.room.send("playerUpdate", localPlayerState);

      this.room.state.players.forEach((playerState, clientSessionId) => {
        const playerObj = this.playerObjs[clientSessionId];
        if (!playerObj) return;
        if (clientSessionId === this.room.sessionId) return;

        tempVector.copy(playerState.position);
        tempVector.y -= 0.6;
        playerObj.position.copy(tempVector);
        playerObj.quaternion.copy(projectToFloor(playerState.quaternion));

        if (playerObj.userData.avatarLoaded) {
          const avatarRoot = playerObj.userData.avatarRoot;
          setToWorldSpace(avatarRoot.userData.head, playerState.position, playerState.quaternion);
          setToWorldSpace(
            avatarRoot.userData.leftHand,
            playerState.controller1Position,
            playerState.controller1Quaternion,
            leftPositionOffset,
            leftRotationOffset
          );
          setToWorldSpace(
            avatarRoot.userData.rightHand,
            playerState.controller0Position,
            playerState.controller0Quaternion,
            rightPositionOffset,
            rightRotationOffset
          );
          const image = playerObj.userData.avatarMesh.material.map.image;
          const mapSrc = image && image.src;
          if (playerState.skin && mapSrc && mapSrc !== playerState.skin) {
            loadSkin(playerObj, playerState.skin);
          }
        }
      });

      for (let i = 0; i < this.room.state.enemies.length; i++) {
        const enemyState = this.room.state.enemies[i];

        if (!this.enemyObjs[i]) {
          this.enemyObjs[i] = this.makeEnemy(enemyState.position, enemyState.quaternion, enemyState.type);
          this.enemyObjs[i].userData.body.body.setUserIndex(i);
        }
        const enemyObj = this.enemyObjs[i];

        this.updateEnemyObjFromState(enemyObj, enemyState);
      }

      this.detectWeaponContact(this.controller0.userData.body, localPlayerPosition, 0, 10);
      this.detectWeaponContact(this.controller1.userData.body, localPlayerPosition, 1, 10);

      const avatarRootUserData = this.playerObj.userData.avatarRoot.userData;
      const leftWeapon = avatarRootUserData.leftGrip.userData.weapon;
      this.detectWeaponContact(leftWeapon.userData.body, localPlayerPosition, 1, leftWeapon.userData.damage);
      const rightWeapon = avatarRootUserData.rightGrip.userData.weapon;
      this.detectWeaponContact(rightWeapon.userData.body, localPlayerPosition, 0, rightWeapon.userData.damage);
    };
  })();

  updateButtonStates() {
    for (let i = 0; i < this.enemyButtonGroup.buttons.length; i++) {
      const button = this.enemyButtonGroup.buttons[i];
      const enemyCost = enemyTypes[i].cost;
      button.setEnabled(this.power < enemyCost);
    }
  }

  loop = (() => {
    const raycaster = new Raycaster();

    const clock = new Clock();

    return () => {
      const delta = clock.getDelta();
      const inVRMode = this.renderer.xr.isPresenting;

      if (inVRMode) {
        this.hud.position.y = -0.5;
        this.hud.position.x = 0.07;
        raycaster.ray.origin.set(0, 0, 0);
        raycaster.ray.direction.set(0, 0, -1);
        raycaster.ray.applyMatrix4(this.activeController.matrix);

        this.playerObj.userData.avatarMesh.material = this.playerObj.userData.origAvatarMat;
        this.playerObj.scale.setScalar(1);
        this.scene.add(this.playerObj);
      } else {
        this.hud.position.z = -1;
        this.hud.position.y = -0.2;
        raycaster.setFromCamera(this.mouse, this.camera);

        this.playerObj.userData.avatarRoot.userData.head.position.setScalar(0);
        this.playerObj.userData.avatarRoot.userData.head.quaternion.identity();
        this.playerObj.userData.avatarRoot.userData.leftHand.position.set(-0.4, -0.4, 0);
        this.playerObj.userData.avatarRoot.userData.leftHand.quaternion.identity();
        this.playerObj.userData.avatarRoot.userData.rightHand.position.set(0.4, -0.4, 0);
        this.playerObj.userData.avatarRoot.userData.rightHand.quaternion.identity();

        this.playerObj.userData.avatarMesh.material = this.playerObj.userData.basicAvatarMat;

        this.camera.add(this.playerObj);
        this.playerObj.position.set(0, 0.3, -1);
        this.playerObj.scale.setScalar(0.15);
        this.playerObj.lookAt(this.camera.position);
      }

      this.intersects.length = 0;
      raycaster.intersectObjects(this.toIntersect, true, this.intersects);

      if (this.intersects.length) {
        this.pointerObj.position.copy(this.intersects[0].point);
      }

      if (inVRMode) {
        updateBody(this.controller0.userData.body, this.controller0);
        updateBody(this.controller1.userData.body, this.controller1);

        const rightWeapon = this.playerObj.userData.avatarRoot.userData.rightGrip.userData.weapon;
        updateBody(rightWeapon.userData.body, rightWeapon.userData.bodyOrigin, true);

        const leftWeapon = this.playerObj.userData.avatarRoot.userData.leftGrip.userData.weapon;
        updateBody(leftWeapon.userData.body, leftWeapon.userData.bodyOrigin, true);
      }

      if (this.room) {
        this.updateRoomState();
      }

      this.world.step();

      if (Date.now() - this.lastPowerIncrement > 1000) {
        this.power = Math.min(this.power + 50, 9999);
        this.lastPowerIncrement = Date.now();
      }
      this.updateButtonStates();
      this.powerText.setText(this.power);

      this.hitSpriteSystem.tick(delta);

      TWEEN.update();

      this.renderer.render(this.scene, this.camera);
    };
  })();

  makeWeaponBody(size) {
    const body = this.world.add({
      group: collisionGroups.weapons,
      mask: collisionGroups.enemies,
      size,
      mass: 1,
      pos: [0, 0, 0],
      kinematic: true,
    });

    return body;
  }

  makeLocalController(index) {
    const obj = this.renderer.xr.getController(index);
    this.scene.add(obj);
    obj.addEventListener("selectstart", () => {
      this.activeController = obj;
    });
    obj.userData.body = this.makeWeaponBody([0.1, 0.1, 0.1]);

    return obj;
  }

  makeWeapon = (() => {
    const weapons = {
      sword: {
        url: "assets/sword.glb",
        damage: 10,
        scale: 0.3,
        bodyOffset: 0.25,
        bodySize: [0.5, 0.1, 0.1],
      },
      bigSword: {
        url: "assets/sword.glb",
        damage: 50,
        scale: 0.5,
        bodyOffset: 0.25,
        bodySize: [0.9, 0.2, 0.2],
      },
    };
    return async (name, local) => {
      const group = new Object3D();

      const weaponConfig = weapons[name];

      const weapon = (await loadModel(weaponConfig.url)).scene;
      weapon.scale.setScalar(weaponConfig.scale);
      group.add(weapon);

      const bodyOrigin = new Object3D();
      bodyOrigin.position.x = weaponConfig.bodyOffset;
      group.add(bodyOrigin);
      group.userData.bodyOrigin = bodyOrigin;

      group.userData.body = this.makeWeaponBody(weaponConfig.bodySize);
      group.userData.damage = weaponConfig.damage;

      return group;
    };
  })();

  initScene() {
    this.scene.fog = new FogExp2(0x0, 0.02);

    const loader = new TextureLoader();
    const netherack = loader.load("assets/netherack.png");
    netherack.repeat.setScalar(100);
    netherack.wrapT = RepeatWrapping;
    netherack.wrapS = RepeatWrapping;

    const floor = new Mesh(new PlaneGeometry(100, 100), new MeshStandardMaterial({ map: netherack }));
    floor.name = "floor";
    floor.rotation.x = -Math.PI / 2;
    floor.receiveShadow = true;
    this.scene.add(floor);
    this.toIntersect.push(floor);

    this.makeRing(8);

    const light = new DirectionalLight();
    light.name = "directional light";
    light.shadow.mapSize.width = light.shadow.mapSize.height = 512;
    light.shadow.normalBias = 0.1;
    light.shadow.camera.top = 10;
    light.shadow.camera.right = 10;
    light.shadow.camera.bottom = -10;
    light.shadow.camera.left = -10;
    light.castShadow = true;
    light.position.set(3, 5, -3);
    light.position.multiplyScalar(2);
    this.scene.add(light);

    this.scene.add(new AmbientLight(0x3f3f3f));

    this.camera = new PerspectiveCamera();
    this.camera.name = "camera";
    this.camera.near = 0.1;
    this.camera.position.set(6, 6, 6);
    this.camera.lookAt(this.scene.position);
    this.camera.add(this.playerObj);
    this.camera.add(this.listener);
    this.scene.add(this.camera);

    const controls = new OrbitControls(this.camera, document.body);
    controls.mouseButtons.LEFT = null;
    controls.mouseButtons.RIGHT = MOUSE.ROTATE;
    controls.enablePan = false;
  }

  async init() {
    window.game = this;
    this.room = null;

    this.scene = new Scene();

    this.playerObj = window.playerObj = await this.makePlayer();
    this.pointerObj = this.makePointer();
    this.playerObjs = {};
    this.enemyObjs = [];
    this.power = 100;
    this.powerText = new Text();
    this.powerText.name = "power text";
    this.lastPowerIncrement = Date.now();
    this.selectedEnemyType = enemyTypes[0].type;

    this.intersects = [];
    this.toIntersect = [];
    this.mouse = new Vector2();

    const skinId = new URLSearchParams(location.search).get("skinId");
    if (skinId) {
      loadFromSkinId(this.playerObj, skinId);
    }

    this.world = await AmmoWorld.createWorld();

    const sword = await this.makeWeapon("sword");
    const bigSword = await this.makeWeapon("bigSword");
    this.playerObj.userData.avatarRoot.userData.rightGrip.add(sword);
    this.playerObj.userData.avatarRoot.userData.rightGrip.userData.weapon = sword;
    this.playerObj.userData.avatarRoot.userData.leftGrip.add(bigSword);
    this.playerObj.userData.avatarRoot.userData.leftGrip.userData.weapon = bigSword;

    this.hitSpriteSystem = new HitSpriteSystem(this.scene);

    this.renderer = window.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.xr.enabled = true;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = VSMShadowMap;
    document.body.append(this.renderer.domElement);

    AudioContext.getContext().suspend();
    document.body.addEventListener("click", async () => {
      AudioContext.getContext().resume();
    });

    this.listener = new AudioListener();

    this.initScene();

    const join = document.getElementById("join");
    join.addEventListener("click", async () => {
      AudioContext.getContext().resume();
      let stream;
      try {
        stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      } finally {
        this.connectClient(stream);
      }
    });
    this.connectClient();

    window.disableReload = false;
    watchFiles(["main.js", "index.html"]);

    window.addEventListener("resize", this.resize.bind(this));
    this.resize();

    this.hud = new Hud();
    this.hud.name = "hud";
    this.hud.addItem(this.powerText);
    this.enemyButtonGroup = new ButtonGroup(
      enemyTypes.map((enemyType) => new Button(makeCube(0.1, enemyType.model, false, true), enemyType.cost.toString()))
    );
    this.hud.addItem(this.enemyButtonGroup);
    this.toIntersect.push(this.hud);
    this.camera.add(this.hud);

    document.body.addEventListener("mousemove", (e) => {
      this.mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
      this.mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;
    });

    const enemyPosition = [];
    document.body.addEventListener("mousedown", (e) => {
      const intersected = this.intersects.length && this.intersects[0];
      if (e.button !== MOUSE.LEFT || !intersected || !this.room) return;

      const canPlace = this.selectedEnemyType !== null && this.power >= enemyByType[this.selectedEnemyType].cost;
      if (intersected.object.name === "floor" && canPlace) {
        intersected.point.toArray(enemyPosition);
        this.room.send("enemyPlaced", {
          position: enemyPosition,
          type: this.selectedEnemyType,
        });
        this.power -= enemyByType[this.selectedEnemyType].cost;
      } else {
        const selectedButtonIndex = ButtonGroup.trySelectButton(intersected.object);
        if (selectedButtonIndex !== null) {
          this.selectedEnemyType = enemyTypes[selectedButtonIndex].type;
        }
      }
    });

    this.controller0 = window.controller0 = this.makeLocalController(0);
    this.controller1 = window.controller1 = this.makeLocalController(1);

    this.activeController = this.controller0;

    this.renderer.setAnimationLoop(this.loop.bind(this));

    document.body.addEventListener("dragover", (e) => e.preventDefault());
    document.body.addEventListener("drop", (e) => {
      e.preventDefault();
      loadSkinFromDataTransfer(this.playerObj, e.dataTransfer);
    });
    document.body.addEventListener("paste", (e) => {
      e.preventDefault();
      loadSkinFromDataTransfer(this.playerObj, e.clipboardData);
    });

    /*
    this.updateEnemyObjFromState(this.makeEnemy({ x: 0, y: 0, z: 0 }, { x: 0, y: 0, z: 0, w: 1 }, "normal"), {
      alive: true,
      position: { x: 0.5, y: 0, z: -0.5 },
      quaternion: { x: 0, y: 0, z: 0, w: 1 },
    });
    */
  }
}

new Game().init();
