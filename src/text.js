import { Mesh, MeshBasicMaterial, PlaneGeometry, Object3D, Texture } from "three";

export class TextCanvas {
  constructor(width, height) {
    this.background = "white";
    this.color = "black";
    this.canvas = document.createElement("canvas");
    this.canvas.width = width;
    this.canvas.height = height;
    this.ctx = this.canvas.getContext("2d");
    this.ctx.font = `bold ${this.canvas.height * 0.8}px sans-serif`;
    this.ctx.textAlign = "center";
    this.ctx.textBaseline = "middle";
    this.texture = new Texture(this.canvas);
    this.text = "";
  }
  setText(text) {
    this.text = text;
    this.update();
  }
  update() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    if (this.background !== "transparent") {
      this.ctx.lineWidth = 10;
      this.ctx.strokeStyle = this.background;
      this.ctx.strokeText(this.text, this.canvas.width / 2, this.canvas.height / 2);
    }

    this.ctx.fillStyle = this.color;
    this.ctx.fillText(this.text, this.canvas.width / 2, this.canvas.height / 2);

    this.texture.needsUpdate = true;
  }
}

export class Text extends Object3D {
  constructor() {
    super();
    this.type = "Text";
    this.textCanvas = new TextCanvas(128, 64);
    const plane = new Mesh(
      new PlaneGeometry(0.1, (0.1 * this.textCanvas.canvas.height) / this.textCanvas.canvas.width),
      new MeshBasicMaterial({ map: this.textCanvas.texture })
    );
    plane.name = "Text plane";
    plane.material.alphaTest = 0.5;
    plane.material.transparent = true;
    this.add(plane);
  }
  setText(text) {
    this.textCanvas.setText(text);
  }
  getWidth() {
    return 0.1;
  }
}
