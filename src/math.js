import { Vector3, Matrix4, Quaternion } from "three";

export const setToWorldSpace = (() => {
  const tempPos = new Vector3();
  const tempPos2 = new Vector3();
  const tempMatrix = new Matrix4();
  const tempQuat = new Quaternion();
  const oneEighty = new Quaternion();
  oneEighty.setFromAxisAngle(new Vector3(0, 1, 0), Math.PI);
  return (obj, pos, quat, positionOffset, rotationOffset) => {
    obj.updateWorldMatrix(true, false);

    tempPos.copy(pos);
    obj.parent.worldToLocal(tempPos);
    obj.position.copy(tempPos);

    tempMatrix.getInverse(obj.parent.matrixWorld);
    obj.quaternion.setFromRotationMatrix(tempMatrix);
    tempQuat.set(quat.x, quat.y, quat.z, quat.w);
    obj.quaternion.multiply(tempQuat);
    obj.quaternion.multiply(oneEighty);
    if (rotationOffset) {
      obj.quaternion.multiply(rotationOffset);
    }
    if (positionOffset) {
      tempPos2.copy(positionOffset);
      tempPos2.applyQuaternion(obj.quaternion);
      obj.position.add(tempPos2);
    }
  };
})();

export const projectToFloor = (() => {
  const forward = new Vector3(0, 0, 1);
  const up = new Vector3(0, 1, 0);
  const tempVector = new Vector3();
  const tempQuat = new Quaternion();
  return (quat) => {
    tempVector.copy(forward);
    tempVector.applyQuaternion(quat);
    tempVector.projectOnPlane(up);
    tempVector.normalize();
    tempVector.negate();
    tempQuat.setFromUnitVectors(forward, tempVector);
    return tempQuat;
  };
})();
