import { Sprite, SpriteMaterial } from "three";
import { TextCanvas } from "./text.js";

export default class HitSpriteSystem {
  constructor(scene) {
    this.scene = scene;
    this.spritePool = [];
    this.sprites = [];
    this.textures = new Map();
    this.addToPool(10);
  }
  addToPool(num) {
    for (let i = 0; i < num; i++) {
      const sprite = new Sprite();
      sprite.visible = false;
      sprite.scale.set(0.1, 0.1, 1);
      sprite.userData.duration = 2;
      this.scene.add(sprite);
      this.spritePool.push(sprite);
    }
  }
  getOrCreateTexture(val) {
    if (!this.textures.get(val)) {
      const textCanvas = new TextCanvas(128, 128);
      textCanvas.background = "transparent";
      textCanvas.color = "green";
      textCanvas.setText(val);
      this.textures.set(val, textCanvas.texture);
    }
    return this.textures.get(val);
  }
  display(val, x, y, z) {
    if (!this.spritePool.length) {
      this.addToPool(this.sprites.length);
    }
    const sprite = this.spritePool.pop();
    sprite.visible = true;
    sprite.material = new SpriteMaterial({ map: this.getOrCreateTexture(val) });
    sprite.material.sizeAttenuation = false;
    sprite.position.set(x, y, z);
    sprite.userData.ttl = 2;
    this.sprites.push(sprite);
  }
  tick(delta) {
    for (let i = this.sprites.length - 1; i >= 0; i--) {
      const sprite = this.sprites[i];
      if (sprite.userData.ttl > 0) {
        sprite.position.y += 0.01;
        sprite.userData.ttl -= delta;
        sprite.material.opacity = sprite.userData.ttl / sprite.userData.duration;
      } else {
        this.sprites.splice(i, 1);
        sprite.visible = false;
        this.spritePool.push(sprite);
      }
    }
  }
}
