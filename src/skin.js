import { TextureLoader } from "three";

export const loadSkin = (() => {
  const loader = new TextureLoader();

  function replaceTexture(material, texture) {
    const oldMap = material.map;
    material.map = texture;
    material.map.flipY = oldMap.flipY;
    material.map.format = oldMap.format;
    material.map.magFilter = oldMap.magFilter;
    material.map.minFilter = oldMap.minFilter;
    material.map.type = oldMap.type;
    material.map.wrapS = oldMap.wrapS;
    material.map.wrapT = oldMap.wrapT;
    material.map.needsUpdate = true;
    material.needsUpdate = true;
  }

  return (playerObj, url) => {
    if (url.startsWith("blob")) return;
    loader.load(url, (texture) => {
      replaceTexture(playerObj.userData.origAvatarMat, texture);
      replaceTexture(playerObj.userData.basicAvatarMat, texture);
    });
  };
})();

export function loadFromSkinId(playerObj, skinId) {
  const url = `https://www.minecraftskins.com/skin/download/${skinId}`;
  const proxied = `${location.origin}/proxy/${encodeURIComponent(url)}`;
  loadSkin(playerObj, proxied);
  history.pushState(null, null, `?skinId=${skinId}`);
  const transfer = document.getElementById("transfer");
  transfer.href = `https://ggff.io/${location.href}`;
}

export function loadSkinFromDataTransfer(playerObj, dataTransfer) {
  let data;
  if (dataTransfer.types.includes("text/uri-list")) {
    data = dataTransfer.getData("text/uri-list");
  } else if (dataTransfer.types.includes("text/plain")) {
    data = dataTransfer.getData("text/plain");
  }
  let url;
  if (data) {
    try {
      url = new URL(data);
    } catch {
      // invalid url
    }
  }
  if (url && url.hostname === "www.minecraftskins.com" && /^\/skin\/\d+/.test(url.pathname)) {
    const skinId = url.pathname.split("/")[2];
    loadFromSkinId(playerObj, skinId);
  }
}
