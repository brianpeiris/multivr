const colyseus = require("colyseus");
const schema = require("@colyseus/schema");
const monitor = require("@colyseus/monitor").monitor;
const { Schema, MapSchema, ArraySchema } = schema;
const { vec3, quat } = require("gl-matrix");
const https = require("https");
const express = require("express");
const fetch = require("node-fetch");
const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const port = process.env.port || 7878;

const createHTTPSConfig = require("./https.js");

const app = express();
app.use(express.static("./"));
const webpackConfig = {
  mode: "development",
  entry: "./src/client.js",
  devtool: "inline-source-map",
  optimization: {
    splitChunks: { chunks: "all" },
  },
  module: {
    rules: [{ test: /\.js$/, exclude: /node_modules/, use: { loader: "babel-loader" } }],
  },
};
app.use(webpackDevMiddleware(webpack(webpackConfig)));
app.use(express.json());

app.get("/proxy/:url", (req, res) => {
  fetch(req.params.url).then((r) => r.body.pipe(res));
});

const gameServer = new colyseus.Server({
  server: https.createServer(createHTTPSConfig(), app),
});

class Vector3 extends Schema {
  constructor(x, y, z) {
    super();
    this.x = x;
    this.y = y;
    this.z = z;
    this.arr = [x, y, z];
  }
  asArray() {
    this.arr[0] = this.x;
    this.arr[1] = this.y;
    this.arr[2] = this.z;
    return this.arr;
  }
  add(vec) {
    if (vec.length) {
      this.x += vec[0];
      this.y += vec[1];
      this.z += vec[2];
    } else {
      this.x += vec.x;
      this.y += vec.y;
      this.z += vec.z;
    }
  }
  lengthSq() {
    return vec3.squaredLength(this.asArray());
  }
}
schema.defineTypes(Vector3, {
  x: "number",
  y: "number",
  z: "number",
});

class Quaternion extends Schema {
  constructor(x, y, z, w) {
    super();
    this.x = x || 0;
    this.y = y || 0;
    this.z = z || 0;
    this.w = w || 1;
    this.arr = [x, y, z, w];
  }
  asArray() {
    this.arr[0] = this.x;
    this.arr[1] = this.y;
    this.arr[2] = this.z;
    this.arr[3] = this.w;
    return this.arr;
  }
  fromArray(arr) {
    this.x = arr[0];
    this.y = arr[1];
    this.z = arr[2];
    this.w = arr[3];
  }
}
schema.defineTypes(Quaternion, {
  x: "number",
  y: "number",
  z: "number",
  w: "number",
});

class Player extends Schema {
  constructor() {
    super();
    this.skin = "";
    this.position = new Vector3();
    this.quaternion = new Quaternion();
    this.controller0Position = new Vector3();
    this.controller0Quaternion = new Quaternion();
    this.controller1Position = new Vector3();
    this.controller1Quaternion = new Quaternion();
  }
}
schema.defineTypes(Player, {
  skin: "string",
  position: Vector3,
  quaternion: Quaternion,
  controller0Position: Vector3,
  controller0Quaternion: Quaternion,
  controller1Position: Vector3,
  controller1Quaternion: Quaternion,
});

class Enemy extends Schema {
  constructor(position, type) {
    super();
    this.health = 100;
    this.alive = true;
    this.type = type;
    this.position = position;
    this.quaternion = new Quaternion();
    this.lookAtOrigin();
  }
  lookAtOrigin = (() => {
    const dir = [0, 0, 0];
    const forward = [0, 0, 1];
    const tempQuat = [0, 0, 0, 1];
    return () => {
      vec3.copy(dir, this.position.asArray());
      vec3.scale(dir, dir, -1);
      vec3.normalize(dir, dir);
      quat.rotationTo(tempQuat, forward, dir);
      this.quaternion.fromArray(tempQuat);
    };
  })();
  moveForward = (() => {
    const forward = [0, 0, 1];
    return () => {
      forward[0] = 0;
      forward[1] = 0;
      forward[2] = this.type === "normal" ? 0.1 : 0.2;
      vec3.transformQuat(forward, forward, this.quaternion.asArray());
      this.position.add(forward);
      if (this.position.lengthSq() > 1000) {
        this.alive = false;
      }
    };
  })();
  registerHit(damage) {
    this.health -= damage;
    if (this.health <= 0) {
      this.alive = false;
    }
  }
}
schema.defineTypes(Enemy, {
  alive: "boolean",
  health: "number",
  type: "string",
  position: Vector3,
  quaternion: Quaternion,
});

class GameState extends Schema {
  constructor() {
    super();
    this.players = new MapSchema();
    this.enemies = new ArraySchema();
  }
}
schema.defineTypes(GameState, {
  players: { map: Player },
  enemies: { array: Enemy },
});

class GameRoom extends colyseus.Room {
  onCreate() {
    this.setState(new GameState());
    this.onMessage("playerUpdate", this.onPlayerUpdate.bind(this));
    this.onMessage("enemyPlaced", this.onEnemyPlaced.bind(this));
    this.onMessage("enemyHit", this.onEnemyHit.bind(this));
    setInterval(this.update.bind(this), 100);
  }
  update() {
    for (const enemy of this.state.enemies) {
      if (enemy.alive) {
        enemy.moveForward();
      }
    }
  }
  onEnemyPlaced(client, message) {
    const position = new Vector3(...message.position);
    this.state.enemies.push(new Enemy(position, message.type));
  }
  onEnemyHit(client, message) {
    console.log("enemyHit", message);
    this.state.enemies.at(message.index).registerHit(message.damage);
  }
  onPlayerUpdate(client, message) {
    let player = this.state.players.get(client.sessionId);
    if (!player) {
      player = new Player();
    }
    player.skin = message.skin;
    player.position.x = message.position[0];
    player.position.y = message.position[1];
    player.position.z = message.position[2];
    player.quaternion.x = message.quaternion[0];
    player.quaternion.y = message.quaternion[1];
    player.quaternion.z = message.quaternion[2];
    player.quaternion.w = message.quaternion[3];
    player.controller0Position.x = message.controller0Position[0];
    player.controller0Position.y = message.controller0Position[1];
    player.controller0Position.z = message.controller0Position[2];
    player.controller0Quaternion.x = message.controller0Quaternion[0];
    player.controller0Quaternion.y = message.controller0Quaternion[1];
    player.controller0Quaternion.z = message.controller0Quaternion[2];
    player.controller0Quaternion.w = message.controller0Quaternion[3];
    player.controller1Position.x = message.controller1Position[0];
    player.controller1Position.y = message.controller1Position[1];
    player.controller1Position.z = message.controller1Position[2];
    player.controller1Quaternion.x = message.controller1Quaternion[0];
    player.controller1Quaternion.y = message.controller1Quaternion[1];
    player.controller1Quaternion.z = message.controller1Quaternion[2];
    player.controller1Quaternion.w = message.controller1Quaternion[3];
    this.state.players.set(client.sessionId, player);
  }
  onJoin(client) {
    console.log(`${client.sessionId} joined`);
  }
  onLeave(client) {
    this.state.players.delete(client.sessionId);
  }
}

gameServer.define("GameRoom", GameRoom);

app.use("/colyseus", monitor());

gameServer.listen(port).then(() => console.log(`Listening on ${port}`));
