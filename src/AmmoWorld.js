let Ammo;

class AmmoBody {
  constructor(world, params) {
    this.world = world;
    this.contactCallback = null;
    this.lastContact = { x: 0, y: 0, z: 0 };
    this.inContact = false;
    this.lastContactBodyUserIndex = null;
    this.contactCount = 0;

    const shape = new Ammo.btBoxShape(new Ammo.btVector3(params.size[0] / 2, params.size[1] / 2, params.size[2] / 2));

    const isDynamic = params.mass !== 0;
    const localInertia = new Ammo.btVector3(0, 0, 0);
    if (isDynamic) shape.calculateLocalInertia(params.mass, localInertia);

    const startTransform = new Ammo.btTransform();
    startTransform.setIdentity();
    startTransform.setOrigin(new Ammo.btVector3(params.pos[0], params.pos[1], params.pos[2]));
    if (!params.rot) params.rot = [0, 0, 0, 1];
    startTransform.setRotation(new Ammo.btQuaternion(params.rot[0], params.rot[1], params.rot[2], params.rot[3]));
    const state = new Ammo.btDefaultMotionState(startTransform);

    const bodyInfo = new Ammo.btRigidBodyConstructionInfo(params.mass, state, shape, localInertia);
    this.body = new Ammo.btRigidBody(bodyInfo);
    if (params.kinematic) {
      this.body.setCollisionFlags(2);
      this.body.setActivationState(4);
    }
    this.body.activate();

    this.transform = new Ammo.btTransform();
    this.position = { x: 0, y: 0, z: 0 };
    this.quaternion = { x: 0, y: 0, z: 0, w: 1 };
  }

  setPosition(pos) {
    const motionState = this.body.getMotionState();
    if (motionState) {
      motionState.getWorldTransform(this.transform);
      const origin = this.transform.getOrigin();
      origin.setX(pos.x);
      origin.setY(pos.y);
      origin.setZ(pos.z);
      motionState.setWorldTransform(this.transform);
    }
  }

  setQuaternion(quaternion) {
    const motionState = this.body.getMotionState();
    if (motionState) {
      motionState.getWorldTransform(this.transform);
      const rotation = this.transform.getRotation();
      rotation.setX(quaternion.x);
      rotation.setY(quaternion.y);
      rotation.setZ(quaternion.z);
      rotation.setW(quaternion.w);
      this.transform.setRotation(rotation);
      motionState.setWorldTransform(this.transform);
    }
  }

  getPosition() {
    const motionState = this.body.getMotionState();
    if (motionState) {
      motionState.getWorldTransform(this.transform);
      const origin = this.transform.getOrigin();
      this.position.x = origin.x();
      this.position.y = origin.y();
      this.position.z = origin.z();
    }
    return this.position;
  }

  getQuaternion() {
    const motionState = this.body.getMotionState();
    if (motionState) {
      motionState.getWorldTransform(this.transform);
      const quaternion = this.transform.getRotation();
      this.quaternion.x = quaternion.x();
      this.quaternion.y = quaternion.y();
      this.quaternion.z = quaternion.z();
      this.quaternion.w = quaternion.w();
    }
    return this.quaternion;
  }

  gainedContact = (() => {
    const ammoContactCallback = new Ammo.ConcreteContactResultCallback();
    ammoContactCallback.addSingleResult = (pointPtr, bodyAPtr, partA, indexA, bodyBPtr) => {
      const point = Ammo.wrapPointer(pointPtr, Ammo.btManifoldPoint);
      const bodyB = Ammo.wrapPointer(bodyBPtr, Ammo.btCollisionObjectWrapper).getCollisionObject();
      const positionA = point.getPositionWorldOnA();
      this.lastContact.x = positionA.x();
      this.lastContact.y = positionA.y();
      this.lastContact.z = positionA.z();

      this.lastContactBodyUserIndex = bodyB.getUserIndex();
      this.contactCount++;
    };

    return () => {
      this.contactCount = 0;
      this.world.contactTest(this.body, ammoContactCallback);
      if (!this.inContact && this.contactCount > 0) {
        this.inContact = true;
        return true;
      }
      if (this.contactCount === 0) {
        this.inContact = false;
      }
      return false;
    };
  })();
}

export default class AmmoWorld {
  constructor() {
    const collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
    const dispatcher = new Ammo.btCollisionDispatcher(collisionConfiguration);
    const overlappingPairCache = new Ammo.btDbvtBroadphase();
    const solver = new Ammo.btSequentialImpulseConstraintSolver();
    this.world = new Ammo.btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
    this.world.setGravity(new Ammo.btVector3(0, -1, 0));
  }

  add(params) {
    const body = new AmmoBody(this.world, params);
    if (params.group !== undefined && params.mask !== undefined) {
      this.world.addRigidBody(body.body, params.group, params.mask);
    } else {
      this.world.addRigidBody(body.body);
    }
    return body;
  }

  remove(body) {
    this.world.removeRigidBody(body.body);
  }

  step() {
    this.world.stepSimulation(1 / 60, 10);
  }

  getContact() {}

  static async createWorld() {
    Ammo = await window.Ammo();
    return new AmmoWorld();
  }
}
