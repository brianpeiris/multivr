const fs = require("fs");
const path = require("path");
const selfsigned = require("selfsigned");

module.exports = function createHTTPSConfig() {
  const cwd = process.cwd();
  if (fs.existsSync(path.join(cwd, "certs"))) {
    const key = fs.readFileSync(path.join(cwd, "certs", "key.pem"));
    const cert = fs.readFileSync(path.join(cwd, "certs", "cert.pem"));

    return { key, cert };
  } else {
    const pems = selfsigned.generate(
      [
        {
          name: "commonName",
          value: "localhost",
        },
      ],
      {
        days: 365,
        keySize: 2048,
        algorithm: "sha256",
      }
    );

    fs.mkdirSync(path.join(cwd, "certs"));
    fs.writeFileSync(path.join(cwd, "certs", "cert.pem"), pems.cert);
    fs.writeFileSync(path.join(cwd, "certs", "key.pem"), pems.private);

    return {
      key: pems.private,
      cert: pems.cert,
    };
  }
};
