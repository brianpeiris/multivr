import { Mesh, MeshBasicMaterial, Object3D, Box3, BackSide, SphereGeometry } from "three";

import { Text } from "./text.js";

export class Button extends Object3D {
  constructor(model, labelText) {
    super();
    this.type = "Button";
    this.model = model;
    this.label = new Text();
    this.label.setText(labelText);
    this.label.position.y = -0.1;
    this.label.position.z = 0.05;
    this.add(this.label);
    this.add(model);
    this.box = new Box3();
    this.box.setFromObject(this);
  }
  getWidth() {
    return this.box.max.x - this.box.min.x;
  }
  setEnabled(enabled) {
    if (enabled) {
      this.label.textCanvas.color = "grey";
    } else {
      this.label.textCanvas.color = "black";
    }
    this.label.textCanvas.update();
  }
}

export class ButtonGroup extends Object3D {
  constructor(buttons) {
    super();
    this.type = "ButtonGroup";
    this.buttons = buttons;
    this.selectionSphere = new Mesh(
      new SphereGeometry(0.09, 16, 16),
      new MeshBasicMaterial({ color: "#1097eb", side: BackSide })
    );
    this.selectionSphere.name = "selection sphere";
    this.selectionSphere.visible = false;

    const group = new Object3D();
    group.name = "ButtonGroup group";
    group.add(this.selectionSphere);
    group.add(buttons[0]);
    let offset = 0;
    const margin = 0.05;
    for (let i = 1; i < buttons.length; i++) {
      group.add(buttons[i]);
      const previousButtonHalfWidth = buttons[i - 1].getWidth() / 2;
      const currentButtonHalfWidth = buttons[i].getWidth() / 2;
      offset += previousButtonHalfWidth + currentButtonHalfWidth + margin;
      buttons[i].position.x = offset;
    }
    group.position.x = -offset / 2;
    const firstButtonHalfWidth = buttons[0].getWidth() / 2;
    const lastButtonHalfWidth = buttons[buttons.length - 1].getWidth() / 2;
    this.width = offset + firstButtonHalfWidth + lastButtonHalfWidth;

    this.add(group);

    this.setSelected(buttons[0]);
  }
  getWidth() {
    return this.width;
  }
  setSelected(button) {
    let buttonIndex = 0;
    let offset = 0;
    const margin = 0.05;
    while (buttonIndex < this.buttons.length) {
      const buttonWidth = this.buttons[buttonIndex].getWidth();
      if (this.buttons[buttonIndex] === button) {
        this.selectionSphere.position.x = offset;
        this.selectionSphere.visible = true;
        return buttonIndex;
      }
      offset += buttonWidth + margin;
      buttonIndex++;
    }
    return null;
  }
  static trySelectButton(obj) {
    let button;
    let buttonGroup;
    let current = obj;
    while (current) {
      if (!button && current.type === "Button") {
        button = current;
      }
      if (!buttonGroup && current.type === "ButtonGroup") {
        buttonGroup = current;
      }
      current = current.parent;
    }
    if (button && buttonGroup) {
      return buttonGroup.setSelected(button);
    }
    return null;
  }
}
