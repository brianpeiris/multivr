import { Object3D } from "three";

export default class Hud extends Object3D {
  constructor() {
    super();
    this.type = "Hud";
    this.items = [];
    this.group = new Object3D();
    this.group.name = "hud group";
    this.add(this.group);
  }
  addItem(item) {
    this.items.push(item);
    this.group.children.length = 0;
    this.group.add(this.items[0]);
    let offset = this.items[0].getWidth() / 2;
    this.items[0].position.x = offset;
    const margin = 0.05;
    for (let i = 1; i < this.items.length; i++) {
      this.group.add(this.items[i]);
      offset += this.items[i - 1].getWidth() / 2 + this.items[i].getWidth() / 2 + margin;
      this.items[i].position.x = offset;
    }
    const width = offset + this.items[this.items.length - 1].getWidth() / 2;
    this.group.position.x = -width / 2;
  }
}
