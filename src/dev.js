async function fetchEtag(file) {
  return (await fetch(file, { method: "HEAD" })).headers.get("etag");
}

export async function watchFiles(files) {
  const etags = {};
  for (const file of files) {
    etags[file] = await fetchEtag(file);
  }
  const checkEtags = async () => {
    for (const file of files) {
      const currentEtag = await fetchEtag(file);
      if (currentEtag !== etags[file]) {
        location.reload();
        return;
      }
    }
    if (!window.disableReload) {
      setTimeout(checkEtags, 1000);
    }
  };
  setTimeout(checkEtags, 1000);
}

export const printParents = (window.printParents = (object3D) => {
  let parents = "";
  let current = object3D;
  while (current) {
    const pos = current.position;
    const quat = current.quaternion;
    parents +=
      `[${current.name}]<${current.type}> | ` +
      `${pos.x.toFixed(2)} ${pos.y.toFixed(2)} ${pos.z.toFixed(2)} | ` +
      `${quat.x.toFixed(2)} ${quat.y.toFixed(2)} ${quat.z.toFixed(2)} ${quat.w.toFixed(2)}\n`;
    current = current.parent;
  }
  console.log(parents);
});
