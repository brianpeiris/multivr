# TODO
- points
- sound effects
- restrict enemy placement to min radius

## done
- make sure voice still works
- swords
- avatars
- positional audio
- haptics

## ideas
- tower defense?
- asymmetric?
- two people in vr, vs 1 person in vr, two people on desktop
 
- throwing weapons, swords
  - shuriken, kunai, katana, executioner's blade, chakra boost?
- enemies also throw weapons or swing swords
  - kunai shooter, katana spinner, bomb

- constant points increase
- points on hit
- lose points when you're hit
- health bar and points bar?
- teleportation?
- thumbstick movement?

## framework
- simplified server code framework
- voice and avatar networking
- individual rooms
- ecsy, typescript?
- tied to three.js?
